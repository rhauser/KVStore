
#include "KVStore/Store.h"

#include "ipc/Partition.hpp"
#include "zipc_rpc/Client.h"

#include <iostream>

struct MyData {
    int x;
    float y;
    std::string z;
    MSGPACK_DEFINE(x,y,z);
};

int main()
{
    try {
        tdaq::ipc::Partition p;
        tdaq::kv::Store store(p.lookup("Store"));
        
        std::cout << "Objects: ";
        for(auto& e : store.list()) {
            std::cout << e << ", ";
        }
        std::cout << std::endl;
        
        std::cout << "Store has 'string':" << store.has("string") << std::endl;
        
        store.put("hello", "world");
        store.put("test_number", 123);
        
        std::cout << "Store has 'test_number':" << store.has("test_number") << std::endl;
        std::cout << "test_number = " << store.get<int>("test_number") << std::endl;
        
        store.put("test_string", "hi there");
        
        std::vector<int> data{1,2,3,4};
        store.put("test_array", data);
        
        std::map<std::string, int> map_data{ { "hi", 10}, {"test", 20 }};
        store.put("test_map", map_data);
        
        auto map_result = store.get<std::map<std::string, int> >("test_map");
        std::cout << "map[hi] = " << map_result["hi"] << " map[test] = " << map_result["test"] << std::endl;
        
        std::cout << "test_string = " << store.get<std::string>("test_string") << std::endl;
        std::cout << "test_array = [";
        for(auto& x : store.get<std::vector<int> >("test_array")) {
            std::cout << x << ", ";
        }
        std::cout << "]\n";
        
        MyData d{10, 3.14, "Hello world"};
        
        store.put("mydata", d);
        
        MyData result = store.get<MyData>("mydata");
        std::cout << "MyData: x = " << result.x << " y = " << result.y << " z = " << result.z << std::endl;

        // the following only works with the Python KVServer
        auto methods = store.call<std::vector<std::string> >("__list");
        if(std::find(methods.begin(), methods.end(), "append") != methods.end()) {

            // clear it up first
            store.remove("some_server");

            store.append("some_server", "tcp://msu-pc1.cern.ch:1234");
            store.append("some_server", "tcp://msu-pc2.cern.ch:314");
            store.append("some_server", "tcp://msu-pc3.cern.ch:3452");
            store.append("some_server", "tcp://msu-pc4.cern.ch:9789");

            std::cout << "Any server: " << store.get_any<std::string>("some_server") << std::endl;
            std::cout << "Any server: " << store.get_any<std::string>("some_server") << std::endl;
            std::cout << "Any server: " << store.get_any<std::string>("some_server") << std::endl;

            std::cout << "Erasing: " << store.erase("some_server",  "tcp://msu-pc3.cern.ch:3452") << std::endl;
            std::cout << "After erase: ";
            for(auto& x : store.get<std::vector<std::string> >("some_server")) {
                std::cout << x << ", ";
            }
            std::cout << std::endl;
        }

    } catch(std::exception& ex) {
        std::cerr << "An exception occured: " << ex.what() << std::endl;
    }
}
