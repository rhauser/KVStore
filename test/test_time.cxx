
#include "KVStore/Store.h"

#include "ipc/Partition.hpp"
#include "zipc_rpc/Client.h"

#include <iostream>
#include <chrono>
#include <string>

struct MyData {
    int x;
    float y;
    std::string z;
    MSGPACK_DEFINE(x,y,z);
};

int main(int argc, char *argv[])
{
    if(argc != 2) {
        std::cerr << "Usage: test_time <count>" << std::endl;
        return 1;
    }

    try {

        using namespace std::chrono;

        tdaq::ipc::Partition p;
        tdaq::kv::Store store(p.lookup("Store"));
        
        int counter = std::stoi(argv[1]);

        // ------------------------
        auto start = high_resolution_clock::now();
        for(int i = 0; i< counter; i++) {
            store.put("hello","world");
        }
        std::cout << "string put:" << duration_cast<milliseconds>(high_resolution_clock::now() - start).count() << " ms" << std::endl;

        // -----------------------
        start = high_resolution_clock::now();
        std::string result;
        for(int i = 0; i< counter; i++) {
            result = store.get<std::string>("hello");
        }
        std::cout << "string get:" << duration_cast<milliseconds>(high_resolution_clock::now() - start).count() << " ms" << std::endl;

        // ------------------------ 
        MyData data{1,3.14,"test"};

        start = high_resolution_clock::now();
        for(int i = 0; i< counter; i++) {
            store.put("userdata", data);
        }
        std::cout << "data put:" << duration_cast<milliseconds>(high_resolution_clock::now() - start).count() << " ms" << std::endl;

    } catch(std::exception& ex) {
        std::cerr << "An exception occured: " << ex.what() << std::endl;
    }
}
