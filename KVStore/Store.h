// this is -*- c++ -*-
#ifndef KVSTORE_STORE_H_
#define KVSTORE_STORE_H_

#include "zipc_rpc/Client.h"

namespace tdaq {

    namespace kv {

        class Store : public tdaq::rpc::Client {
        public:
            Store(const tdaq::ipc::Endpoint& ep)
                : tdaq::rpc::Client(zmq::socket_type::req, ep)
            {
            }

            ~Store() {}

            template<class T>
            void put(const std::string& key, const T& value)
            {
                call<void>("put", key, value);
            }

            bool has(const std::string& key)
            {
                return call<bool>("has", key);
            }

            bool remove(const std::string& key)
            {
                return call<bool>("remove", key);
            }

            std::vector<std::string> 
            list(const std::string& regex = ".*")
            {
                return call<std::vector<std::string>>("list", regex);
            }

            template<class T>
            T get(const std::string& key)
            {
                return call<T>("get", key);
            }

            // Extended interface, only implemented in Python server
            template<class T>
            bool append(const std::string& key, const T& value)
            {
                return call<bool>("append", key, value);
            }

            template<class T>
            bool erase(const std::string& key, const T& value)
            {
                return call<bool>("erase", key, value);
            }

            template<class T>
            T get_any(const std::string& key)
            {
                return call<T>("get_any", key);
            }
        };

    }

}

#endif // KVSTORE_STORE_H_
