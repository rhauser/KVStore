#!/usr/bin/env tdaq_python

import sys
import time

from zipc_rpc import Partition, Client
c = Client(Partition().lookup('Store'))

start = time.time()
for i in range(int(sys.argv[1])):
    c.put('test', 'some string')
print "string:", time.time() - start

start = time.time()
for i in range(int(sys.argv[1])):
    c.put('test', 100)
print "int:", time.time() - start
