#!/usr/bin/env tdaq_python
from zipc_rpc import Server, Partition

import re
import random

class Store:

    def __init__(self):
        self.data = {}
        self.rand = random.Random()

    def put(self, key, value):
        self.data[key] = value

    def append(self, key, value):
        if self.data.has_key(key):
            d = self.data[key]
            if type(d) == list:
                d.append(value)
                return True
            else:
                return False
        else:
            self.data[key] = [value]
            return True

    def erase(self, key, value):
        if self.data.has_key(key) and type(self.data[key]) == list and self.data[key].count(value) > 0:
            self.data[key].remove(value)
            return True
        else:
            return False

    def get(self, key):
        return self.data.get(key)

    def get_any(self, key):
        if self.data.has_key(key) and type(self.data[key]) == list:
            return self.rand.choice(self.data[key])
        else:
            raise KeyError

    def has(self, key):
        return self.data.has_key(key)

    def list(self, regex = '.*'):
        return [ i for i in self.data.iterkeys() if re.match(regex, i) ]

    def remove(self, key):
        if self.data.has_key(key):
            self.data.pop(key)
            return True
        else:
            return False

if __name__ == "__main__":
    
    server = Server(server=Store())
    Partition().publish(server.endpoint, "Store")
    server.run()
