#!/usr/bin/env tdaq_python

from zipc_rpc import Partition, Client

c = Client(Partition().lookup('Store'))

c.put('string','hello world')
c.put('number', 10)
c.put('array', [1,2,3])
c.put('object', { 'a': 10, 'b': [4,5,6]})

print "Objects:", c.list('.*')

print "Store has 'string':",c.has('string')

for i in c.list('.*'):
    print "%s = " % i, c.get(i)

print "Removing 'number':",c.remove('number')
print "Removing 'unknown':",c.remove('unknown')

if 'append' in c.__list():
    c.remove('some_server')
    c.append('some_server', 'tcp://msu-pc1.cern.ch:1234')
    c.append('some_server', 'tcp://msu-pc2.cern.ch:2345')
    c.append('some_server', 'tcp://msu-pc3.cern.ch:4241')
    c.append('some_server', 'tcp://msu-pc4.cern.ch:343')
    c.append('some_server', 'tcp://msu-pc5.cern.ch:7686')

    print "some_server = ",c.get_any('some_server')
    print "some_server = ",c.get_any('some_server')
    print "some_server = ",c.get_any('some_server')
    print "some_server = ",c.get_any('some_server')

    c.erase('some_server', 'tcp://msu-pc5.cern.ch:7686')
    print c.get('some_server')

try:
    # Don't provide parameter for list method which is required on the C++ side
    print "Objects again:", c.list()
except Exception as ex:
    print "list() call resulted in exception: ", ex
