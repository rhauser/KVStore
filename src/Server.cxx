
#include "Server.h"
#include "ipc/Partition.hpp"

#include <regex>

namespace tdaq {

    namespace kv {

        Server::Server(int nthreads)
            : tdaq::rpc::Server(zmq::socket_type::rep, nthreads)
        {
            add_method("put", &Server::put, this, std::placeholders::_1, std::placeholders::_2);
            add_method("has", &Server::has, this, std::placeholders::_1);
            add_method("remove", &Server::remove, this, std::placeholders::_1);
            add_method("list", &Server::list, this, std::placeholders::_1);
            add_method("get", &Server::get, this, std::placeholders::_1);
        }
        
        Server::~Server()
        {}
        
        void Server::put(const std::string& key, const msgpack::object& value)
        {
            std::unique_lock<std::mutex> lock(m_mutex);
            m_data[key] = msgpack::clone(value);
        }

        bool Server::has(const std::string& key)
        {
            std::unique_lock<std::mutex> lock(m_mutex);
            return m_data.find(key) != m_data.end();
        }
        
        bool Server::remove(const std::string& key) 
        {
            std::unique_lock<std::mutex> lock(m_mutex);
            auto it = m_data.find(key);
            if(it != m_data.end()) {
                m_data.erase(it);
                return true;
            }
            return false;
        }
        
        std::vector<std::string>
        Server::list(const std::string& regex)
        {
            std::vector<std::string> result;

            std::regex rex(regex);

            std::unique_lock<std::mutex> lock(m_mutex);
            for(auto& i : m_data) {
                if(std::regex_match(i.first, rex)) {
                    result.push_back(i.first);
                }
            }
            return  result;
        }

        msgpack::object
        Server::get(const std::string& key)
        {
            std::unique_lock<std::mutex> lock(m_mutex);
            auto it = m_data.find(key);
            if(it != m_data.end())  {
                return it->second.get();
            }
            throw std::domain_error("Key not found: " + key);
        }
        
    }
}

int main(int argc, char *argv[])
{
    int nthreads = 1;

    if(argc > 1) {
        nthreads = std::stoi(argv[1]);
    }

    tdaq::kv::Server server(nthreads);
    tdaq::ipc::Partition partition;
    partition.publish(server, "Store");

    while(true) {
        pause();
    }
}
