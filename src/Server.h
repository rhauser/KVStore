// this is -*- c++ -*-
#ifndef KV_SERVER_H_
#define KV_SERVER_H_

#include <string>
#include <map>
#include <vector>
#include <mutex>

#include "zipc_rpc/Server.h"

namespace tdaq {

    namespace kv {

        class Server : public tdaq::rpc::Server {
        public:
            Server(int nthreads = 1);
            ~Server();

            void put(const std::string& key, const msgpack::object& value);
            bool has(const std::string& key);
            bool remove(const std::string& key) ;
            std::vector<std::string> list(const std::string& regex);
            msgpack::object get(const std::string& key);
        private:
            std::mutex                                    m_mutex;
            std::map<std::string, msgpack::object_handle> m_data;
        };

    }

}

#endif // KV_SERVER_H_
